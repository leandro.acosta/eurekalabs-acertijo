from PIL import Image, ImageColor
import requests

url = "http://52.8.212.143:6431"
req = requests.get(url=url).json()
height = req['size']['height']
width = req['size']['width']
data = req['data']

img = Image.new('RGB', (height, width), "black")
pixels = img.load()

for i in range(img.size[0]):
    for j in range(img.size[1]):
        if data[i * img.size[1] + j] == 0:
            pixels[i, j] = (255, 255, 255)
        else:
            pixels[i, j] = (0, 0, 0)

img = img.transpose(Image.FLIP_LEFT_RIGHT).transpose(Image.ROTATE_90)

img.show()
